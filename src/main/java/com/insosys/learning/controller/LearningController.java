package com.insosys.learning.controller;

import com.insosys.learning.dto.Person;
import com.insosys.learning.service.LearningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class LearningController {

    @Autowired
    LearningService learningService;

    @GetMapping(value = "/getpersonname")
    public String getPersonName(@RequestParam (value = "gender",defaultValue = "gender") String gender){
    return learningService.getName(gender);
    }



}
