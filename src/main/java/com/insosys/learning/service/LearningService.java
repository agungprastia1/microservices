package com.insosys.learning.service;

import com.insosys.learning.dto.Person;
import org.springframework.stereotype.Service;

@Service
public class LearningService {
    public String getName(String gender){
        Person person = new Person();

        if ("man".equals(gender)){
            person.setName("Jhon");
        }else{
            person.setName("Jane");
        }

        return person.getName();
    }


}
